# \_\_main\_\_.py usage in python

This file is used, when running pyhton with `-m` statement.
If `-m` is not present, it won't work properly.

see https://docs.python.org/3/library/__main__.html