# pyxamples

Python examples of basic functionalities of libraries I use. (nothing special)
I make them for myself for better understanding of the usage of library
 (may not be sometimes correct).

## pytest

All basic features should be covered.

Run, by `pytest <directory_with_tests>` - runs all files with prefix `test_*`

Covered issues:

* basic test
  * assert
  * error expectation (negative testing)
  * modules - clustering tests
  * parametrization - exhausting testing
* mentioned:
  * fixtures
  * capsys
  * monkeypatch - faster testing somehow (swapping some package for different)

### pytest/fixtures

Testing default fixtures and also creating of new ones (dependent on style)

### Links

* [youtube]("https://www.youtube.com/watch?v=fv259R38gqc")
* [official docs]("https://docs.pytest.org/en/latest/contents.html")