import sys
import argparse
import re

def arg_fcn(value):
    '''test function
    '''
    print('RUNING ARG FUNCTION')
    print(value)
    
    if re.match('^\d+.\d*$', value):
        print('is number')
        value = float(value)
        return value * 5
    else:
        return str(value) + ' was the value'

def main():
    top_parser = argparse.ArgumentParser(description='Argument parser basic')
    
    top_parser.add_argument('something')
    top_parser.add_argument('-inspect', '-i')
    
    # arg func type
    top_parser.add_argument(
            '-arg-fcn',
            type=arg_fcn,
            default=None,
            )

    arguments=top_parser.parse_args()
    print(arguments)

if __name__ == '__main__':
    #sys.argv.append('some-basic-shit')
    #sys.argv.append('1234')
    #sys.argv.append('-inspect')
    #sys.argv.append('IMAGE')
    #sys.argv.append('785')
    print(sys.argv)
    main()
