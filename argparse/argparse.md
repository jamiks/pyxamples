# Argparse cheatsheet

- [Argparse cheatsheet](#argparse-cheatsheet)
  - [ArgumentParser](#argumentparser)
    - [add_argument](#add_argument)
    - [add_subparsers](#add_subparsers)

## ArgumentParser

### add_argument

`name` or `flags`
: Either a name or a list of option strings, e.g. foo or `-f`, `--foo`.

`action`
: The basic type of action to be taken when this argument is encountered at the command line.
    - `store` - default action, stores the value as string `foo='42'`
    - `store_const` - stores value that is defined by `const` value
        > must also contain **`const`** option
    - `store_true` and `store_false` - stores true or false
    - `append` - when option is specified multiple times and it appends it as list
    - `append_const` - appends constant into list defined by `const` option.
        > must also contain **`const`** option
    - `count` - counts the number of times a keyword argument occurs
        > counts value from `default` option, which is by default None
    - `help` - useless. Just prints the complete help for all options used on current oarser.
    - `version` - prints whatever is defined in `version` option. It can be `'%(prog)'` which prints the value of `ArgumentParser.prog`.
        > uses `version` option also
    - `extend` - extends each argument except the specified one into list.
    - you can also use some special action - see docs[^1] for that

`nargs`
: The number of command-line arguments that should be consumed.
    - can be some of following values:
      - `'N'` or number - N items will be added into list
      - `'?'` - one value if not present uses `default` when not present and `cost` when no values are present.
        > uses **`default`** and **`const`**
      - `'*'` - adds arguments into list. When none, list is empty.
      - `'+'` - adds arguments into list. When none, error is risen.

`const`
: A constant value required by some action and nargs selections. Default is _`None`_
    - Used when options present:
      - `nargs='?'`
      - `action='append_const`
      - `action='store_const`

`default`
: The value when the argument is absent. Default is _`None`_
    - can have value `argparse.SUPPRESS` which makes to suppres creating of the variable for this option when empty.

`type`
: The type to which the command-line argument should be converted.
    - `open` - opens the file for reading (i think.. dunno)
    - `argparse.Filetype('option')` - opens also the file, with option set to w, r etc.
    - can take any callable, that takes single string argument and returns converted value

`choices`
: A container of the allowable values for the argument. Usually a list of strings (or other values)

`required`
: Whether or not the command-line option may be omitted (optionals only).
    - `True` or `False` value

`help`
: A brief description of what the argument does.
    - access to parameters using `help='some %(parameter/argument_name)s value`

`metavar`
: A name for the argument in usage messages.


`dest`
: The name of the attribute to be added to the object returned by parse_args().

### add_subparsers

- returns special action object when calling `subparsers = argparse_instance.add_subparsers()`
  - the object has only one op. `add_parser()`
  - the usage is that:
    - define default parser
    - create some subpraser out of it, when the option defined in `add_parser('option')` is present.
    - add some arguments according to general rules.
  - When there is the subparser option present, the parser is then activated and used.

i.e. subparser for adding jam or adding yogurth to pancakes.

```python
pancakes = argparse.ArgumentParser(description='pancake preparator`)
pancakes.add_argument('-cook', type=store_true)

sub_fillings = pancakes.add_subparsers(help='omg, pancakes')

marmelade = sub_fillings.add_parser('marmelade', help='choose marmelade')
marmelade.add_argument('type', choices=['strawberry', 'peach', 'raspberry'])

yogurt = sub_fillings.add_parser('yogurt', help='choose yogurt')
yogurt.add_argument('type', choices=['white', 'strawberry', 'peach', 'choco'])
```



[^1]: https://docs.python.org/3/library/argparse.html