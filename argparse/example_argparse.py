import argparse
import sys
import pdb

def handle_args(args, parser_ret)
    handles 
    if hasattr(args, 'op'):
        handles[args.op].handle_args(args, parser_ret[args.op])
    else:
        parser_ret[None].print_usage()
        sys.exit(1)


def nsfarm_example():
    '''Main function, running always
    '''
    print('Actual sys.argv value: %s' % (sys.argv))
    
    top_parser = argparse.ArgumentParser(description='Argument parser basic')
    
    subparsers = top_parser.add_subparsers()

    ret = {None: top_parser}

    lxd_parser = subparsers.add_parser('lxd', help='Images management')
    lxd_parser.set_defaults(op='lxd')

    subparsers_2 = lxd_parser.add_subparsers()

    clean = subparsers_2.add_parser('clean', help='remove old unused containers')
    clean.set_defaults(lxd_op='clean')
    clean.add_argument(
        'DELTA',
        nargs='?',
        default='some actual date',
        help="""Time delta for how long image should not be used to be cleaned (removed). In default if not specified
        '1w' is used. Format is expect to be a number with suffix. Supported suffixes are m(inute), h(our), d(ay) and
        w(eek).
        """
    )
    clean.add_argument(
        '-n', '--dry-run',
        action='store_true',
        help='Print what would be removed but do nothing.'
    )

    bootstrap = subparsers.add_parser('bootstrap', help='Bootstrap specific or all images')
    bootstrap.set_defaults(lxd_op='bootstrap')
    bootstrap.add_argument(
        'IMG',
        nargs='*',
        help='Image to bootstrap.'
    )
    bootstrap.add_argument(
        '-a', '--all',
        action='store_true',
        help='Bootstrap all images present instead of only listed ones.'
    )

    inspect = subparsers.add_parser(
        'inspect',
        help="Create new container from given image and access shell. You can use this to inspect image's content.")
    inspect.set_defaults(lxd_op='inspect')
    inspect.add_argument(
        'IMAGE',
        help="""
        """
    )

    ret["lxd"] = {None: lxd_parser,
                'clean' : clean,
                'bootstrap' : bootstrap,
                'inspect' : inspect}

    handles = {'lxd' : 'shhh',}

    args = top_parser.parse_args()
    parser_ret = ret

    
    top_parser.add_argument('-sh')
    a=top_parser.parse_args()

    print(a)
    pdb.set_trace()

def main():
    top_parser = argparse.ArgumentParser(description='Argument parser basic')
    

if __name__ == '__main__':
    sys.argv.append('op')
    sys.argv.append('lxd')
    #sys.argv.append('inspect')
    #sys.argv.append('IMAGE')
    #sys.argv.append('')
    print(sys.argv)
    main()