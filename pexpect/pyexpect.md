# Pyexpect

## Overview

- use for `ssh` connection

## Most used objects

### `pexpect.spawn(<command>)`

creates class [spawn]()

```mermaid
classDiagram
    class spawn
    spawn : <<class>>
    spawn : (command, )
    spawn : before: str
    spawn : after: str
    spawn : send(str)
    spawn : sendline(str)
    spawn : expect(str regex)
```

- `send`
- `sendline`
- `expect`
- `logfile_read` and `logfile_send`
- `read_nonblocking`
- 

## References

- [API Overview](https://pexpect.readthedocs.io/en/stable/overview.html)
- 