# Relative imports (python)

## Usage

folder structure:
```
└── project
    ├── package1
    │   ├── module1.py
    │   └── module2.py
    └── package2
        ├── __init__.py
        ├── module3.py
        ├── module4.py
        └── subpackage1
            └── module5.py
```

in module2.py importing module5.py:
```python
from ..package2.subpackage import module5
```