from .lib import __main__ as lib


def printer():
    print('__main__ name:')
    print(__name__)
    print('lib __name__:')
    print(lib.__name__)

if __name__ == '__main__':
    printer()