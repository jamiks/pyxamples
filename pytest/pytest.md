# Pytest

## Running pytest

- use `pytest -v` to see more detail info about tests
- use `pytest -v --capture=no` for debugging and see all prints that are done in tests

## Structure

- Tests
  - Test must be named with prefix `test_` for automatic run.
- Fixtures
  - see -> [fixtures](./fixtures.md)

## References

- **Test styles:**
  - [xunit-style](https://docs.pytest.org/en/stable/xunit_setup.html)
  - [unittest.TestCase style](https://docs.pytest.org/en/stable/unittest.html)
  - [Nose style](https://docs.pytest.org/en/stable/nose.html)