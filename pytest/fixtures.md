# Fixtures

## Overview

In short - fixture is basically pre and post conditioning.

## Syntax

Fixture has to have decorator `@pytese.fixture` additionally, it may have some
arguments to it. Most used (in farm):

`name`
: Just name of the fixture.

`scope`
: Defines scope - when the fixture is executed - for example `function` is only
  executed only once a `test_`... function is executed, while `module` is 
  executed only once a `test_`...`.py` is executed.
  Possible values:
    - `function`
    - `class`
    - `module`
    - `package`
    - `session`
    - [_**dynamic**_](https://docs.pytest.org/en/stable/fixture.html#dynamic-scope)

`params`
: list of parameters that are processed and then test is run for every each one
  one of them.
  - additionally there can be added `ids` argument, that can be 

`autouse`
: If True uses the fixture for every test which can see this fixture.

## Using fixtures

See following files for code examples:
    - `./conftest.py`
    - `./test_fixtures.py`
      - run the test or play with the fixtures here.

Fixture uses return or yeld to pass some value, method, class to the test.
To use some variables/constants/lists or loggers, with only one instance use the
scope attribute of the fixture (see also [Syntax](#syntax)):
```python
@pytest.fixture(scope='session')
```
```python
import pytest


@pytest.fixture
def my_fixture():
    # code to be executed before scope
    yeld value # or function also can be used return, but then after-code wont 
               # be executed
    # code to be executed after scope

def test_ehlo(my_fixture):
    print( my_fixture) # shows the value
    ...
```

### Using fixtures with mark

Except the default usage (as arguments) you can use fixtures using:

```python
@pytest.mark.usefixtures("fixture_name")
```

Then the fixture is used on the following class.
The fixture then can contain `request` argument and use `request.cls` to work
with the class that it's attached to. i.e. create attibutes etc.

## References

- [Fixtures docs](https://docs.pytest.org/en/stable/fixture.html)
- [API reference](https://docs.pytest.org/en/stable/reference.html)
- [Very good guide with examples](https://www.lambdatest.com/blog/end-to-end-tutorial-for-pytest-fixtures-with-examples/)