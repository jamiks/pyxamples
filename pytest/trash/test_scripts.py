import json
import pytest

#tested file
import scripts

def test_proof():
    assert True

class TestAdding:

    @pytest.mark.parametrize(
        'values',
        [
            ([4,7,9,10]),
            ([1,2]),
            ([7.7,1.8,2])
        ],)
    def test_adding_basic(self, values):
        expected = sum(values)
        assert scripts.adding(*values) == expected

    @pytest.mark.parametrize(
        'value', [
            ('string'),
            ('string'),
            ([1]),
            ({'value' : 1}),
            ((1.2,1))])
    def test_addind_input(self, value):
        with pytest.raises(TypeError):
            scripts.adding(value)


def test_multiply_basic():
    assert scripts.multiply(4,7,5) == 4*7*5

def test_fixture(my_fixture):
    '''prepares the environment
    '''
    assert my_fixture == 42

def test_capsys(capsys):
    '''get the stdout as output
    '''
    print('hello')
    out, err = capsys.readouterr()
    assert "hello\n" == out
    assert '' == err

def test_monkeypatch(monkeypatch):
    """fakes some function/method
    for integration testing pretty good
    """
    def fake_add(a, b):
        return 42
    monkeypatch.setattr(scripts, "adding", fake_add)
    assert scripts.adding(2, 3) == 42

def test_tmpdir(tmpdir):
    '''Creates temporary file
    '''
    some_file = tmpdir.join('tmpdir_text.txt')
    some_file.write('{"hello":"world"}')

    result = scripts.read_json(str(some_file))
    assert result["hello"] == "world"

def test_fixture_with_fixtures(capsys, captures_print):
    print("more")
    out, err = capsys.readouterr()
    assert out == "hello\nmore\n"

