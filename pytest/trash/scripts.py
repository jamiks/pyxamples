import random
import scapy
import json

class WrongTypeError(Exception):
    pass

def adding(*args):
    out = 0
    for i in args:
        if isinstance(i, (int, float)):
            out += i
        else:
            raise TypeError('Wrong type! Duh..')
    return out

def multiply(*args):
    out = 1
    for i in args:
        if isinstance(i, (int,)):
            out = out * i
        else:
            raise TypeError('Rwong type! Duh..')
    return out

def get_random_number(min, max):
    return random.randrange(min, max)*1.1

if __name__ == '__main__':
    print('This is running as main.')

def read_json(file_path):
    with open(file_path,'r') as f:
        return json.load(f)


