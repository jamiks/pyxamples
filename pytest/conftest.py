import pytest

a = 0

@pytest.fixture(name="pre")
def precondition():
    print("## precondition print")
    return "## precondition returns"


@pytest.fixture(name="post")
def postcondition():

    yield "## postcondition yield"
    print("## postcondition print")


@pytest.fixture
def pre_and_post():
    '''Without name
    '''
    print("## pre_and_post print 1")
    yield "## pre_and_post yeld"
    print("## pre_and_post print 2")

# To show different scopes:

@pytest.fixture(scope='function')
def scope_fcn():
    a = ['scope_fcn']
    print("** scope fcn pre")
    yield a
    print("** scope fcn post")

@pytest.fixture(scope='session')
def scope_sess():
    a = ['scope_session']
    print("** scope session pre")
    yield a
    print("** scope session post")

# using parameters attr

@pytest.fixture(params=["run 1", "run 2", "run 3"])
def fx_params(request):
    '''using parameters

    Here it's about using different functions/values according to test setup
    for different tests.

    The test will be done for each of the params inside the list
    '''

    print(f'input param is "{request.param}"')
    print("** scope session pre")
    yield request.param
    print("** scope session post")

@pytest.fixture(scope='module', name='fx_out')
def fx_out():
    print('\n out of fcn pre\n')
    return False