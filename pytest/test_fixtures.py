'''Example using fixtures

demonstration of fixtures
'''
import pytest

pytest.mark.usefixtures('fx_out')

def test_fx_call_1(pre, post, pre_and_post, scope_fcn, scope_sess):
    '''Calling fixtures as test arguments
    '''
    
    scope_sess.append('first_call')
    scope_fcn.append('first_call')
    print(scope_fcn)
    print(scope_sess)

    print('******')
    print(pre)
    print(post)
    print(pre_and_post)

    assert True

def test_fx_call_2(pre_and_post, scope_fcn, scope_sess):
    '''Calling fixtures for second time
    '''
    scope_sess.append('second_call')
    scope_fcn.append('second_call')

    print(scope_fcn)
    print(scope_sess)
    print(pre_and_post)
    assert True

#@pytest.mark.usefixtures("fx_params")
def test_fx_call_params(fx_params):
    '''Parametrized test
    
    In here the fixture is adding different values into the function.
    '''
    print(fx_params)
    assert True

def test_no_fix():
    print('there is no fixture')
    assert True